import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import '../components/default_button.dart';
import '../components/rounded_icon_btn.dart';
import '../const/AppColors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../constants.dart';
import '../screens/details/components/top_rounded_container.dart';
import '../size_config.dart';

class ProductDetails extends StatefulWidget {
  var _product;
  ProductDetails(this._product);
  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  Future addToCart() async {
    final FirebaseAuth _auth = FirebaseAuth.instance;
    var currentUser = _auth.currentUser;
    CollectionReference _collectionRef =
        FirebaseFirestore.instance.collection("users-cart-items");

    // Mencari produk dengan nama yang sama di dalam keranjang
    QuerySnapshot snapshot = await _collectionRef
        .doc(currentUser!.email)
        .collection("items")
        .where("name", isEqualTo: widget._product["product-name"])
        .get();

    if (snapshot.docs.isNotEmpty) {
      // Jika produk dengan nama yang sama sudah ada, tambahkan jumlah produk yang baru
      int existingJumlah = snapshot.docs[0]["jumlah"];
      int newJumlah = existingJumlah + jumlah;

      // Perbarui jumlah dan total harga produk yang sudah ada di dalam keranjang
      await _collectionRef
          .doc(currentUser!.email)
          .collection("items")
          .doc(snapshot.docs[0].id)
          .update({
        "jumlah": newJumlah,
        "price": widget._product["product-price"] * newJumlah,
      });
    } else {
      // Jika produk belum ada di dalam keranjang, tambahkan produk baru
      await _collectionRef
          .doc(currentUser!.email)
          .collection("items")
          .doc()
          .set({
        "name": widget._product["product-name"],
        "jumlah": jumlah,
        "price": widget._product["product-price"] * jumlah,
        "images": widget._product["product-img"],
      });
    }

    print("Added to cart");
    // Tampilkan pemberitahuan jika data berhasil ditambahkan
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text("Data berhasil ditambahkan ke keranjang belanja."),
      ),
    );
  }

  Future<void> addToFavourite() async {
    final FirebaseAuth _auth = FirebaseAuth.instance;
    var currentUser = _auth.currentUser;

    CollectionReference _collectionRef =
        FirebaseFirestore.instance.collection("users-favourite-items");

    await _collectionRef.doc(currentUser!.email).collection("items").add({
      "name": widget._product["product-name"],
      "price": widget._product["product-price"],
      "images": widget._product["product-img"],
      // Tambahkan informasi lain yang ingin Anda simpan sebagai favorit
    });

    // Tampilkan pemberitahuan jika berhasil ditambahkan ke daftar favorit
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(content: Text("Berhasil ditambahkan ke daftar favorit.")),
    );
  }

  Future<void> removeFromFavourite() async {
    final FirebaseAuth _auth = FirebaseAuth.instance;
    var currentUser = _auth.currentUser;

    CollectionReference _collectionRef =
        FirebaseFirestore.instance.collection("users-favourite-items");

    // Ambil dokumen yang ingin dihapus berdasarkan nama produk
    var querySnapshot = await _collectionRef
        .doc(currentUser!.email)
        .collection("items")
        .where("name", isEqualTo: widget._product['product-name'])
        .get();

    // Hapus semua dokumen yang sesuai dari koleksi
    for (var doc in querySnapshot.docs) {
      await doc.reference.delete();
    }

    // Tampilkan pemberitahuan jika berhasil dihapus dari daftar favorit
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(content: Text("Dihapus dari daftar favorit.")),
    );
  }

  bool _isExpanded = false;

  int jumlah = 1; // Jumlah awal, bisa diubah sesuai dengan kebutuhan

  void tambahJumlah() {
    setState(() {
      jumlah++; // Memperbarui jumlah ketika ikon "Add" diklik
    });
  }

  void kurangJumlah() {
    if (jumlah > 0) {
      setState(() {
        jumlah--; // Memperbarui jumlah ketika ikon "Remove" diklik
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: Padding(
          padding: const EdgeInsets.all(8.0),
          child: CircleAvatar(
            backgroundColor: AppColors.deep_orange,
            child: IconButton(
                onPressed: () => Navigator.pop(context),
                icon: Icon(
                  Icons.arrow_back,
                  color: Colors.white,
                )),
          ),
        ),
        actions: [
          StreamBuilder(
            stream: FirebaseFirestore.instance
                .collection("users-favourite-items")
                .doc(FirebaseAuth.instance.currentUser!.email)
                .collection("items")
                .where("name", isEqualTo: widget._product['product-name'])
                .snapshots(),
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return CircularProgressIndicator();
              } else if (snapshot.hasError) {
                return Text("Error");
              }

              bool isFavorite = snapshot.data!.docs.isNotEmpty;

              return Padding(
                padding: const EdgeInsets.only(right: 8),
                child: CircleAvatar(
                  backgroundColor: Colors.red,
                  child: IconButton(
                    onPressed: () =>
                        isFavorite ? removeFromFavourite() : addToFavourite(),
                    icon: isFavorite
                        ? Icon(
                            Icons.favorite,
                            color: Colors.white,
                          )
                        : Icon(
                            Icons.favorite_outline,
                            color: Colors.white,
                          ),
                  ),
                ),
              );
            },
          ),
        ],
      ),
      body: SafeArea(
          child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 12),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: getProportionateScreenWidth(double.infinity),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: Image.network(widget._product['product-img']),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: getProportionateScreenWidth(20)),
              child: Text(
                widget._product['product-name'],
                style: Theme.of(context).textTheme.headline6,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                left: getProportionateScreenWidth(20),
                right: getProportionateScreenWidth(64),
              ),
              child: Text(
                widget._product['product-description'],
                maxLines: _isExpanded ? null : 3,
                overflow: _isExpanded ? null : TextOverflow.ellipsis,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(20),
                vertical: 10,
              ),
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    _isExpanded = !_isExpanded;
                  });
                },
                child: Row(
                  children: [
                    Text(
                      _isExpanded ? 'Tampilkan Kurang' : 'Lihat Semua',
                      style: TextStyle(
                          fontWeight: FontWeight.w600, color: kPrimaryColor),
                    ),
                    SizedBox(width: 5),
                    Icon(
                      Icons.arrow_forward_ios,
                      size: 12,
                      color: kPrimaryColor,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      )),
      bottomNavigationBar: Container(
        padding: EdgeInsets.symmetric(
          vertical: getProportionateScreenWidth(15),
          horizontal: getProportionateScreenWidth(30),
        ),
        // height: 174,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30),
            topRight: Radius.circular(30),
          ),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, -15),
              blurRadius: 20,
              color: Color(0xFFDADADA).withOpacity(0.15),
            )
          ],
        ),
        child: SafeArea(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(children: [
                Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Color(0xFFF5F6F9),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Text(jumlah.toString()),
                ),
                Spacer(),
                RoundedIconBtn(
                  icon: Icons.remove,
                  press: kurangJumlah,
                ),
                SizedBox(width: getProportionateScreenWidth(20)),
                RoundedIconBtn(
                  icon: Icons.add,
                  showShadow: true,
                  press: tambahJumlah,
                ),
              ]),
              SizedBox(height: getProportionateScreenHeight(20)),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text.rich(
                    TextSpan(
                      text: "Total:\n",
                      children: [
                        TextSpan(
                          text:
                              "\Rp ${widget._product["product-price"] * jumlah}",
                          style: TextStyle(fontSize: 16, color: Colors.black),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: getProportionateScreenWidth(190),
                    child: DefaultButton(
                      text: "Add to Cart",
                      press: () {
                        addToCart();
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

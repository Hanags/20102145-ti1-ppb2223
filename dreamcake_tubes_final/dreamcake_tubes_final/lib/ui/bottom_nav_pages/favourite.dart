import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import '../../constants.dart';
import '../../size_config.dart';
import '../product_details_screen.dart';

class Favourite extends StatefulWidget {
  @override
  _FavouriteState createState() => _FavouriteState();
}

class _FavouriteState extends State<Favourite> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: StreamBuilder(
            stream: FirebaseFirestore.instance
                .collection('users-favourite-items')
                .doc(FirebaseAuth.instance.currentUser!.email)
                .collection("items")
                .snapshots(),
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: Text("Loading..."),
                );
              } else if (!snapshot.hasData || snapshot.data!.docs.isEmpty) {
                return Center(
                  child: Text("Data kosong"),
                );
              }

              return SizedBox(
                width: MediaQuery.sizeOf(context).width,
                height: MediaQuery.sizeOf(context).height * 2,
                child: MasonryGridView.builder(
                  itemCount: snapshot.data!.docs.length,
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate:
                      const SliverSimpleGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2),
                  itemBuilder: (context, index) {
                    DocumentSnapshot _documentSnapshot =
                        snapshot.data!.docs[index];
                    return Padding(
                      padding: EdgeInsets.only(
                          left: getProportionateScreenWidth(10),
                          right: getProportionateScreenWidth(10)),
                      child: SizedBox(
                        width: getProportionateScreenWidth(16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              padding: EdgeInsets.all(
                                  getProportionateScreenWidth(10)),
                              decoration: BoxDecoration(
                                color: kSecondaryColor.withOpacity(0.1),
                                borderRadius: BorderRadius.circular(15),
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(12),
                                child:
                                    Image.network(_documentSnapshot['images']),
                              ),
                            ),
                            const SizedBox(height: 10),
                            Text(
                              _documentSnapshot['name'],
                              style: TextStyle(color: Colors.black),
                              maxLines: 2,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Rp ${_documentSnapshot['price'].toString()}",
                                  style: TextStyle(
                                    fontSize: getProportionateScreenWidth(18),
                                    fontWeight: FontWeight.w600,
                                    color: kPrimaryColor,
                                  ),
                                ),
                                InkWell(
                                  borderRadius: BorderRadius.circular(50),
                                  onTap: () async {
                                    final FirebaseAuth _auth =
                                        FirebaseAuth.instance;
                                    var currentUser = _auth.currentUser;

                                    CollectionReference _collectionRef =
                                        FirebaseFirestore.instance.collection(
                                            "users-favourite-items");

                                    // Ambil dokumen yang ingin dihapus berdasarkan nama produk
                                    var querySnapshot = await _collectionRef
                                        .doc(currentUser!.email)
                                        .collection("items")
                                        .where("name",
                                            isEqualTo:
                                                _documentSnapshot['name'])
                                        .get();

                                    // Hapus semua dokumen yang sesuai dari koleksi
                                    for (var doc in querySnapshot.docs) {
                                      await doc.reference.delete();
                                    }

                                    // Tampilkan pemberitahuan jika berhasil dihapus dari daftar favorit
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                          content: Text(
                                              "Dihapus dari daftar favorit.")),
                                    );
                                  },
                                  child: Container(
                                    padding: EdgeInsets.all(
                                        getProportionateScreenWidth(8)),
                                    decoration: BoxDecoration(
                                      color: kPrimaryColor.withOpacity(0.15),
                                      shape: BoxShape.circle,
                                    ),
                                    child: Icon(
                                      Icons.favorite,
                                      color: Color(0xFFFF4848),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 10),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../../const/AppColors.dart';
import '../login_screen.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  TextEditingController? _nameController;
  TextEditingController? _phoneController;
  TextEditingController? _ageController;

  setDataToTextField(data) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Nama lengkap',
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ),
        TextFormField(
          style: TextStyle(color: Colors.white), // Ubah warna teks ke putih
          decoration: InputDecoration(
            filled: true,
            fillColor: Color(0xFFEBAC63),
          ),
          controller: _nameController = TextEditingController(
            text: data['name'],
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          'Nomor telepon',
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ),
        TextFormField(
          style: TextStyle(color: Colors.white), // Ubah warna teks ke putih
          decoration: InputDecoration(
            filled: true,
            fillColor: Color(0xFFEBAC63),
          ),
          controller: _phoneController =
              TextEditingController(text: data['phone']),
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          'Umur',
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ),
        TextFormField(
          style: TextStyle(color: Colors.white), // Ubah warna teks ke putih
          decoration: InputDecoration(
            filled: true,
            fillColor: Color(0xFFEBAC63),
          ),
          controller: _ageController = TextEditingController(text: data['age']),
        ),
        SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            ElevatedButton(
              onPressed: () => _showConfirmationDialog(
                context,
                "Konfirmasi Update",
                "Apakah Anda yakin ingin melanjutkan update?",
                updateData, // Panggil updateData jika dikonfirmasi
              ),
              child: Text("Update"),
              style: ElevatedButton.styleFrom(
                primary:
                    AppColors.deep_orange, // Menggunakan AppColors.deep_orange
                onPrimary: Colors.white,
              ),
            ),
            ElevatedButton(
              onPressed: () => _showConfirmationDialog(
                context,
                "Konfirmasi Logout",
                "Apakah Anda yakin ingin logout?",
                () {
                  FirebaseAuth.instance.signOut().then((value) {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) =>
                            LoginScreen(), // Ganti dengan halaman login Anda
                      ),
                    );
                  }).catchError((error) {
                    // Handle error
                    print('Error during logout: $error');
                  });
                }, // Logout jika dikonfirmasi
              ),
              child: Text("Logout"),
              style: ElevatedButton.styleFrom(
                primary:
                    AppColors.deep_orange, // Menggunakan AppColors.deep_orange
                onPrimary: Colors.white,
              ),
            ),
          ],
        ),
      ],
    );
  }

  void _showConfirmationDialog(BuildContext context, String title,
      String message, VoidCallback onConfirmed) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(message),
          actions: <Widget>[
            ElevatedButton(
              onPressed: () {
                onConfirmed(); // Panggil onConfirmed jika dikonfirmasi
                Navigator.of(context).pop(); // Tutup dialog
              },
              child: Text("Ya"),
              style: ElevatedButton.styleFrom(
                primary:
                    AppColors.deep_orange, // Menggunakan AppColors.deep_orange
                onPrimary: Colors.white,
              ),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).pop(); // Tutup dialog
              },
              child: Text("Tidak"),
              style: ElevatedButton.styleFrom(
                primary:
                    AppColors.deep_orange, // Menggunakan AppColors.deep_orange
                onPrimary: Colors.white,
              ),
            ),
          ],
        );
      },
    );
  }

  updateData() {
    CollectionReference _collectionRef =
        FirebaseFirestore.instance.collection("users-form-data");
    return _collectionRef.doc(FirebaseAuth.instance.currentUser!.email).update({
      "name": _nameController!.text,
      "phone": _phoneController!.text,
      "age": _ageController!.text,
    }).then((value) => print("Updated Successfully"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: StreamBuilder(
          stream: FirebaseFirestore.instance
              .collection("users-form-data")
              .doc(FirebaseAuth.instance.currentUser!.email)
              .snapshots(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            var data = snapshot.data;
            if (data == null) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            return setDataToTextField(data);
          },
        ),
      )),
    );
  }
}

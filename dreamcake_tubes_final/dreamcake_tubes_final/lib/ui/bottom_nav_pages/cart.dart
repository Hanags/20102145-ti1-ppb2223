import 'package:url_launcher/url_launcher.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../components/default_button.dart';
import '../../constants.dart';
import '../../size_config.dart';

class Cart extends StatefulWidget {
  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {
  // void sendWhatsApp(String text) {
  //   String url = 'wa.me/62895357948667?text=$text';
  //   launchUrl(Uri.parse(url));
  // }

  void sendWhatsApp(String message) async {
    final link = WhatsAppLink(phoneNumber: '62895357948667', message: message);
    if (await canLaunch(link.toString())) {
      await launch(link.toString());
    } else {
      print('Tidak dapat membuka WhatsApp.');
    }
  }

  String generateInvoice(
      List<QueryDocumentSnapshot> products, int totalProducts, int totalPrice) {
    String invoice = "Halo kak, saya ingin pesan beberapa produk ini!\n\n";

    // Loop untuk setiap produk dalam daftar
    for (var doc in products) {
      String productName = doc["name"];
      int productPrice = doc["price"];
      int productQuantity = doc["jumlah"];

      // Tambahkan detail produk ke invoice
      invoice += "$productName = Rp $productPrice x$productQuantity\n";
    }

    // Tambahkan total jumlah produk dan total harga ke invoice
    invoice += "\nJumlah = $totalProducts\n";
    invoice += "Total = Rp $totalPrice";

    return invoice;
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: StreamBuilder(
            stream: FirebaseFirestore.instance
                .collection('users-cart-items')
                .doc(FirebaseAuth.instance.currentUser!.email)
                .collection("items")
                .snapshots(),
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: Text("Loading..."),
                );
              } else if (!snapshot.hasData || snapshot.data!.docs.isEmpty) {
                return Center(
                  child: Text("Data kosong"),
                );
              }

              return ListView.builder(
                itemCount: snapshot.data!.docs.length,
                itemBuilder: (context, index) {
                  DocumentSnapshot _documentSnapshot =
                      snapshot.data!.docs[index];
                  return Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Dismissible(
                      key: Key(_documentSnapshot['name']),
                      direction: DismissDirection.endToStart,
                      onDismissed: (direction) {
                        setState(() {
                          FirebaseFirestore.instance
                              .collection('users-cart-items')
                              .doc(FirebaseAuth.instance.currentUser!.email)
                              .collection("items")
                              .doc(_documentSnapshot.id)
                              .delete();
                        });
                      },
                      background: Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        decoration: BoxDecoration(
                          color: Color(0xFFFFE6E6),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Row(
                          children: [
                            Spacer(),
                            Icon(Icons.delete)
                            // SvgPicture.asset("assets/icons/Trash.svg"),
                          ],
                        ),
                      ),
                      child: Row(
                        children: [
                          SizedBox(
                            width: 88,
                            child: AspectRatio(
                              aspectRatio: 0.88,
                              child: Container(
                                padding: EdgeInsets.all(
                                    getProportionateScreenWidth(10)),
                                decoration: BoxDecoration(
                                  color: Color(0xFFF5F6F9),
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child:
                                    Image.network(_documentSnapshot['images']),
                              ),
                            ),
                          ),
                          SizedBox(width: 20),
                          SizedBox(
                            width: MediaQuery.sizeOf(context).width / 2,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  _documentSnapshot['name'],
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 16),
                                  maxLines: 2,
                                ),
                                SizedBox(height: 10),
                                Text.rich(
                                  TextSpan(
                                    text: "Rp ${_documentSnapshot['price']}",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        color: kPrimaryColor),
                                    children: [
                                      TextSpan(
                                          text:
                                              " x${_documentSnapshot['jumlah']}",
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                },
              );
            },
          ),
        ),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.symmetric(
          vertical: getProportionateScreenWidth(15),
          horizontal: getProportionateScreenWidth(30),
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30),
            topRight: Radius.circular(30),
          ),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, -15),
              blurRadius: 20,
              color: Color(0xFFDADADA).withOpacity(0.15),
            )
          ],
        ),
        child: StreamBuilder<QuerySnapshot>(
          stream: FirebaseFirestore.instance
              .collection('users-cart-items')
              .doc(FirebaseAuth.instance.currentUser!.email)
              .collection("items")
              .snapshots(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Text("...");
            } else if (!snapshot.hasData || snapshot.data!.docs.isEmpty) {
              return Center(
                child: Text("Data kosong"),
              );
            }

            // Reset nilai jumlahProduk dan totalHarga sebelum menghitung ulang
            int jumlahProduk = 0;
            int totalHarga = 0;

            // Loop untuk menghitung jumlah produk keseluruhan dan total harga
            for (var doc in snapshot.data!.docs) {
              int jumlahProdukItem = doc["jumlah"];
              int hargaProdukItem = doc["price"];
              jumlahProduk += jumlahProdukItem;
              totalHarga += hargaProdukItem;
            }

            return Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text.rich(
                      TextSpan(
                        text: "Jumlah: $jumlahProduk",
                      ),
                    ),
                    Spacer(),
                    Text.rich(
                      TextSpan(
                        text: "Total: Rp $totalHarga",
                      ),
                    ),
                  ],
                ),
                SizedBox(height: getProportionateScreenHeight(20)),
                SizedBox(
                  width: double.infinity,
                  child: DefaultButton(
                    text: "Pesan via WhatsApp",
                    press: () {
                      String invoice = generateInvoice(
                          snapshot.data!.docs, jumlahProduk, totalHarga);
                      sendWhatsApp(invoice);
                    },
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}

class WhatsAppLink {
  final String phoneNumber;
  final String message;

  WhatsAppLink({required this.phoneNumber, required this.message});

  @override
  String toString() {
    String encodedMessage = Uri.encodeComponent(message);
    return 'https://wa.me/$phoneNumber?text=$encodedMessage';
  }
}
